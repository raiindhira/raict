import React, { Component } from "react";
import "./App.css";
import Login from "./pages/component/login/Login";
import SideBar from "./pages/component/base/SideBar";
import Role from "./pages/component/base/role";
import Header from "./pages/component/header/Header";

import Projectdetail from "./pages/component/project/Projectdetail";
import Milestone from "./pages/component/project/Milestone";

import Review from "./pages/component/taskcategory/Review";
import TCdetail from "./pages/component/taskcategory/TCdetail";
import Task from "./pages/component/taskcategory/Task";
import Submission from "./pages/component/taskcategory/Submission";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import MilestoneContainer from "./components/MilestoneContainer";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <div className="some-page-wrapper">
            <div className="grid">
              <div className="grid__item panel-1" />
              <div className="grid__item panel-2">
                <div id="content" className="col-md-12">
                  <ProjectDetail />
                  <div className="row">
                    <Milestone />
                  </div>
                </div>
                <img
                  src="https://i.ibb.co/hdvQWyr/bg1.png"
                  id="bg1"
                  alt="background"
                />
                <img
                  src="https://i.ibb.co/ZH3hZ2Q/bg2.png"
                  id="bg2"
                  alt="background"
                />
              </div>
            </div>
          </div>
        </div>

        <MilestoneContainer />
      </Router>
    );
  }
}

export default App;
