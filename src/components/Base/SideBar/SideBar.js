import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './SideBar.css'

export default class SideBar extends Component {
    render() {
        return (
        <div className="sidebar">
            <div className="row">
                <div className="col-md-4">
                    <img src="logo.png" id="logo" />
                </div>
                <div className="col-xs-8">
                    <p id="sidebar-title">PT GRAPADI SEJAHTERA MANDIRI</p>
                    <p id="title">PROJECT AND PERFORMANCE MANAGEMENT SYSTEM</p>
                </div>
            </div>

            <div className="list-group list-group-flush">
                <a href="#" className="list-group-item list-group-item-action">
                <img src="dashboard.png" id="dashboard" alt="dashboard"></img>
                Dashboard</a>
                <div className="column">
                    <a href="#" className="list-group-item list-group-item-action" id="pj">
                    <img src="project.png" id="project" alt="project"></img>
                    Daftar Proyek
                    <div className="column" id="set">
                    </div>
                    </a>
                </div>
                <a href="#" className="list-group-item list-group-item-action">
                <img src="performa.png" id="performa" alt="performa"></img>
                Performa Staf</a>
                <hr />
            </div>
        </div>
        );
    }
}