import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './Comment.css';
import FormComment from '../../../Forms/Comment/FormComment';
import FormModal from '../../../Forms/Milestone/Modal/FormModal.js';

class Comment extends Component {
    constructor(props){
        super(props);
        this.state = {
            
        };
    }
    render() {
        return (
            <div className="card">
                <div className="card-header" id="byStaff">
                    Bambang - Staff
                </div>
                <div className="card-body">
                    <p id="commentStaff">{this.props.content}</p>
                </div>
                <FormModal flag = {this.props.isUpdate} handleClose = {this.props.hideUpdateFormModal}>
                    <FormComment
                        action = "UPDATE"
                        handleClose = {this.props.hideUpdateFormModal}
                        commentId = {this.props.commentId}
                    />
                </FormModal>
                <button type = "button" onClick = {this.props.showUpdateFormModal}>EDIT</button>
                <button type = "button" onClick = {event => this.props.removeCommentHandler(event, this.props.commentId)}>DELETE</button>
            </div>
        );
    }
}

export default Comment;