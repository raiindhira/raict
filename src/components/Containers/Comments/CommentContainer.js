import React, { Component } from 'react';
import FormComment from '../../Forms/Comment/FormComment.js';
import Axios from 'axios';
import Comment from './Comment/Comment.js';

class CommentContainer extends Component {
    constructor(props){
        super(props);
        this.state = {
            comments: [],
            isUpdate: false       
        };
        this.removeCommentHandler = this.removeCommentHandler.bind(this);
        this.showUpdateFormModal = this.showUpdateFormModal.bind(this);
        this.hideUpdateFormModal = this.hideUpdateFormModal.bind(this);
    }

    async componentDidMount() {
        await Axios.get(`http://localhost:8080/api/taskCategories/${this.props.taskCategoryId}/comments/`)
            .then(response => {
                const fetchResult = response.data;
                this.setState({
                    comments: fetchResult
                })
            })
    }

    showUpdateFormModal = () => {
        this.setState({
            isUpdate: true
        })
    }

    hideUpdateFormModal = () => {
        this.setState({
            isUpdate: false
        })
    }

    async removeCommentHandler(event, commentId) {
        await fetch(`http://localhost:8080/api/comments/${commentId}`, {
            method: 'DELETE',
            headers: {
                'Accept' :  'application/json',
                'Content-Type' : 'application/json'
            }
        })
            .then(() => {
                let updatedComments = [...this.state.comments].filter(i => i.id !== commentId);
                this.setState({
                    comments: updatedComments
                });
            })
    }

    render() {
        return(
            <div>
                <div>
                    <FormComment
                        action = "CREATE"
                        taskCategoryId = {this.props.taskCategoryId}/>
                </div>
                <div id="allComments">
                    {this.state.comments.map(
                        comment =>
                            <Comment
                                isUpdate = {this.state.isUpdate}
                                showUpdateFormModal = {this.showUpdateFormModal}
                                hideUpdateFormModal = {this.hideUpdateFormModal}
                                commentId = {comment.id}
                                removeCommentHandler = {this.removeCommentHandler} 
                                content = {comment.content}/>
                    )}
                </div>
            </div>
        )
    }
}

export default CommentContainer;