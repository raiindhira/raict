import React, { Component } from "react";
import "./Milestone.css";
import { Progress } from "react-sweet-progress";
import "react-sweet-progress/lib/style.css";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel
} from "react-accessible-accordion";
import "react-accessible-accordion/dist/fancy-example.css";
import Axios from "axios";
import FormMilestone from "../../../Forms/Milestone/FormMilestone.js";
import FormModal from "../../../Forms/Milestone/Modal/FormModal.js";
import TaskCategoryContainer from "../../TaskCategories/TaskCategoryContainer";
import Longmenu from "./Longmenu";


class Milestone extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      startDate: "",
      dueDate: ""
    };
  }

  state = {
    showMenuPopup: false
  };

  async componentDidMount() {
    await Axios.get(
      `http://localhost:8080/api/milestones/${this.props.id}`
    ).then(response => {
      const milestone = response.data;
      this.setState({
        name: milestone.name,
        startDate: milestone.startDate,
        dueDate: milestone.endDate
      });
    });
  }

  MenuNav = () => {
    return (
      <i
        className="fas fa-ellipsis-v"
        id="icon"
        onClick={e => {
          this.onShowMenu();
        }}
      >
        {this.state.showMenuPopup && <this.MenuList />}
      </i>
    );
  };

  onShowMenu = e => {
    console.log("HEY menu");
    this.setState({
      showMenuPopup: !this.state.showMenuPopup
    });
  };

  MenuList = () => {
    return (
      <div className="menu-popup">
        <h2>
          <button onClick={this.props.showUpdateFormModal}>EDIT</button>
        </h2>
        <h2>
          <button
            onClick={event =>
              this.props.removeMilestoneHandler(event, this.props.id)
            }
          >
            DELETE
          </button>
        </h2>
      </div>
    );
    //
  };

  render() {
    return (
      <div className="Milestone">
        {/* <div className="listMilestone"> */}

        <div className="row" id="allMilestone">
          <div id="bx-mst" />

          <div className="col-md-12 mstContent">
          <div className="row">
              <div className="col-md-2">
              <h6 id="mst-name">{this.state.name}</h6>
              </div>
              <div className="col-md-2">
              <Progress
                    type="circle"
                    percent={100}
                    width={23}
                    strokeWidth={3}
                    status="default"
                    theme={{
                      default: {
                        symbol: "80",
                        color: "#C4C4C4"
                      }
                    }}
                  />
              </div>    
             <div className="col-md-7">
             <p id="due-mst">Due Date: {this.state.dueDate}</p>
             </div>
          <div className="col-md-1">     
          <Longmenu
            showUpdateFormModal={this.showUpdateFormModal}
            hideUpdateFormModal={this.hideUpdateFormModal}
          />
          </div>
          

          </div>
          <div className="row">
                     <Progress
                percent={100}
                status="success"
                theme={{
                  default: {
                    symbol: "0%",
                    color: "#C4C4C4"
                  },
                  active: {
                    symbol: "50%",
                    color: "#FDDB17"
                  },
                  success: {
                    symbol: "100%",
                    color: "#95BF44"
                  }
                }}
              />
           
          </div>
          

          </div>
        </div>
        {/* </div> */}
      </div>
    );
  }
}

export default Milestone;
