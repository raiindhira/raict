import React, { Component } from "react";
import axios from "axios";
import Milestone from "./Milestone/Milestone";
import FormMilestone from "../../Forms/Milestone/FormMilestone.js";
import FormModal from "../../Forms/Milestone/Modal/FormModal.js";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

class RealMilestoneContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      milestones: [],
      isCreate: false,
      isUpdate: false,
      show: false,
      showForm: false
    };
    this.showCreateFormModal = this.showCreateFormModal.bind(this);
    this.hideCreateFormModal = this.hideCreateFormModal.bind(this);
    this.showUpdateFormModal = this.showUpdateFormModal.bind(this);
    this.hideUpdateFormModal = this.hideUpdateFormModal.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.removeMilestoneHandler = this.removeMilestoneHandler.bind(this);
  }

  componentDidMount() {
    axios
      .get(`http://localhost:8080/api/projects/1/milestones/`)
      .then(response => {
        console.log("MASUK")
        const fetchResult = response.data;
        this.setState({
          milestones: fetchResult
        });
      });
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleCloseUpdate(){
      this.setState({showForm: false})
  }

  handleShow() {
    this.setState({ show: true });
  }

  showCreateFormModal = () => {
    this.setState({ isCreate: true });
  };

  hideCreateFormModal = () => {
    this.setState({ isCreate: false });
  };

  showUpdateFormModal = () => {
    this.setState({ isUpdate: true });
  };

  hideUpdateFormModal = () => {
    this.setState({ isUpdate: false });
  };

  async removeMilestoneHandler(event, milestoneId) {
    await fetch(`http://localhost:8080/api/milestones/${milestoneId}`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    }).then(() => {
      let updatedMilestones = [...this.state.milestones].filter(
        i => i.id !== milestoneId
      );
      this.setState({
        milestones: updatedMilestones
      });
    });
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <h3>Milestone</h3>
          <FormModal
            judul="Tambah Milestone"
            flag={this.state.show}
            handleClose={this.handleClose}
          >
            <FormMilestone
              action="CREATE"
              handleClose={this.hideCreateFormModal}
            />
          </FormModal>
          <FormModal
            judul="Ubah Milestone"
            flag={this.state.showForm}
            handleClose={this.handleCloseUpdate}
          >
            <FormMilestone
              action="UPDATE"
              handleClose={this.showUpdateFormModal}
            />
          </FormModal>

          <div id="create-mst">
            <a onClick={this.handleShow} id="tambah-mst">
              Tambah Milestone
            </a>{" "}
            <a id="plus">+</a>
          </div>
        </div>

        <div className="row">
          <ul>
            {this.state.milestones.map(milestone => (
              <li>
                <div className="row">
                  <Milestone
                    id={milestone.id}
                    removeMilestoneHandler={this.removeMilestoneHandler}
                    isUpdate={this.state.isUpdate}
                    showUpdateFormModal={this.showUpdateFormModal}
                    hideUpdateFormModal={this.hideUpdateFormModal}
                  />
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default RealMilestoneContainer;
