import React, { Component } from 'react';
import "./TaskCategory.css";
import Axios from 'axios';
import FormModal from '../../../Forms/Milestone/Modal/FormModal';
import FormTaskCategory from '../../../Forms/TaskCategory/FormTaskCategory';

class TaskCategory extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: "",
            dueDate: ""
        }
    }

    async componentDidMount() {
        await Axios.get(`http://localhost:8080/api/taskCategories/${this.props.taskCategoryId}`)
            .then(response => {
                const taskCategory = response.data;
                this.setState ({
                    name: taskCategory.name,
                    dueDate: taskCategory.dueDate
                })
            })
    }

    render() {
        return (
            <div>
                <div>
                    <p>{this.state.name}</p>
                    <p>Due Date: {this.state.dueDate}</p>
                </div>
                <div>
                    <FormModal flag = {this.props.isUpdate} handleClose = {this.props.hideUpdateFormModal}>
                        <FormTaskCategory
                            action = "UPDATE"
                            handleClose = {this.props.hideUpdateFormModal}
                            id = {this.props.taskCategoryId}
                        />
                    </FormModal>
                    <button type="button" onClick={this.props.showUpdateFormModal}>EDIT</button>
                    <button type="button" onClick={event => this.props.removeTaskCategoryHandler(event, this.props.taskCategoryId)}>DELETE</button>
                </div>
            </div>
        )
    }
}

export default TaskCategory;