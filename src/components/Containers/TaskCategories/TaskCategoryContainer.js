import React, { Component } from 'react';
import Axios from 'axios';
import TaskCategory from '../TaskCategories/TaskCategory/TaskCategory.js';
import FormModal from '../../Forms/Milestone/Modal/FormModal.js';
import FormTaskCategory from '../../Forms/TaskCategory/FormTaskCategory.js';

class TaskCategoryContainer extends Component {
    constructor(props){
        super(props);
        this.state = {
            taskCategories: [],
            isCreate: false,
            isUpdate: false
        }
        this.showCreateFormModal = this.showCreateFormModal.bind(this);
        this.hideCreateFormModal = this.hideCreateFormModal.bind(this);
        this.showUpdateFormModal = this.showUpdateFormModal.bind(this);
        this.hideUpdateFormModal = this.hideUpdateFormModal.bind(this);
        this.removeTaskCategoryHandler = this.removeTaskCategoryHandler.bind(this);
    }

    async componentDidMount() {
        await Axios.get(`http://localhost:8080/api/milestones/${this.props.milestoneId}/taskCategories/`)
            .then(response => {
                const fetchResult = response.data;
                console.log(fetchResult)
                this.setState({
                    taskCategories: fetchResult
                })
            })
    }

    showCreateFormModal = () => {
        this.setState({
            isCreate: true
        });
    }

    hideCreateFormModal = () => {
        this.setState({
            isCreate: false
        })
    }

    showUpdateFormModal = () => {
        this.setState({
            isUpdate: true
        })
    }

    hideUpdateFormModal = () => {
        this.setState({
            isUpdate: false
        })
    }

    async removeTaskCategoryHandler(event, taskCategoryId) {
        await fetch(`http://localhost:8080/api/taskCategories/${taskCategoryId}`, {
            method: 'DELETE',
            headers: {
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(() => {
                let updatedTaskCategories = [...this.state.taskCategories].filter(i => i.id !== taskCategoryId);
                this.setState({
                    taskCategories: updatedTaskCategories
            });
        })
    }

    render() {
        console.log(this.state.taskCategories)
        return (
            <div>
                <FormModal judul='Tambah Task Category' flag = {this.state.isCreate} handleClose = {this.hideCreateFormModal}>
                    <FormTaskCategory 
                        action = "CREATE"
                        milestoneId = {this.props.milestoneId}
                        handleClose = {this.hideCreateFormModal}
                    />
                </FormModal>
                <div>
                    <a type="button" onClick={this.showCreateFormModal} id="add-tc">Tambah Task Category +</a>
                </div>
                <div>
                    <ul>
                        {this.state.taskCategories.map(
                            taskCategory => 
                                <li>
                                    <TaskCategory 
                                        taskCategoryId = {taskCategory.id}
                                        isUpdate = {this.state.isUpdate}
                                        showUpdateFormModal = {this.showUpdateFormModal}
                                        hideUpdateFormModal = {this.hideUpdateFormModal}
                                        removeTaskCategoryHandler = {this.removeTaskCategoryHandler}
                                    />
                                </li>
                        )}
                    </ul>
                </div>
            </div>
        )
    }
}

export default TaskCategoryContainer;