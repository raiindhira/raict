import React, { Component } from 'react';
import './Task.css';
import FormModal from '../../../Forms/Milestone/Modal/FormModal';
import FormTask from '../../../Forms/Task/FormTask';

class Task extends Component {
    constructor(props) {
        super(props);
        this.state = {
            description: ""
        };
    }

    async componentDidMount() {

    }

    render() {
        return (
            <div className="row">
                <input type="checkbox" id="cb"></input>
                <p id="t" className="col md-3">{this.props.description}</p>
                <FormModal flag = {this.props.isUpdate} handleClose = {this.props.hideUpdateFormModal}>
                    <FormTask 
                        action = "UPDATE"
                        handleClose = {this.props.hideUpdateFormModal}
                        taskId = {this.props.taskId}
                    />
                </FormModal>
                <div id="t-det">
                <button className="col md-3" onClick = {this.props.showUpdateFormModal} id="edit-task">Edit</button>
                <button className="col md-3" onClick = {event => this.props.removeTaskHandler(event, this.props.taskId)} id="hapus-task">Hapus</button>
                </div>
            </div>
        );
    }
}

export default Task;