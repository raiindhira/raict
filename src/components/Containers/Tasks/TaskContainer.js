import React, { Component } from 'react';
import Axios from 'axios';

import Task from './Task/Task.js';
import './TaskContainer.css';
import FormModal from '../../Forms/Milestone/Modal/FormModal.js';
import FormTask from '../../Forms/Task/FormTask.js';

class TaskContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
            isCreate: false,
            isUpdate: false
        };
        this.showCreateFormModal = this.showCreateFormModal.bind(this);
        this.hideCreateFormModal = this.hideCreateFormModal.bind(this);
        this.showUpdateFormModal = this.showUpdateFormModal.bind(this);
        this.hideUpdateFormModal = this.hideUpdateFormModal.bind(this);
        this.removeTaskHandler =  this.removeTaskHandler.bind(this);
    }

    async componentDidMount() {
        await Axios.get(`http://localhost:8080/api/taskCategories/${this.props.taskCategoryId}/tasks`)
            .then(response => {
                const fetchResult = response.data;
                this.setState({
                    tasks: fetchResult
                });
            })
    }

    showCreateFormModal = () => {
        this.setState({ isCreate: true });
    };
    
    hideCreateFormModal = () => {
        this.setState({ isCreate: false });
    };

    showUpdateFormModal = () => {
        this.setState({ isUpdate: true });
    };

    hideUpdateFormModal = () => {
        this.setState({ isUpdate: false });
    }

    async removeTaskHandler(event, taskId) {
        await fetch(`http://localhost:8080/api/tasks/${taskId}`, {
            method: 'DELETE',
            headers: {
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(() => {
                let updatedTasks = [...this.state.tasks].filter(i => i.id !== taskId);
                this.setState({
                    tasks: updatedTasks
                });
            })
    }

    render() {
        return (
            <div>
                <div className="Task">
                    <h3 id="todo">To-Do-List</h3>
                    <hr id="linetd"/>
                    <FormModal flag = {this.state.isCreate} handleClose = {this.hideCreateFormModal}>
                        <FormTask
                            taskCategoryId = {this.props.taskCategoryId}
                            action = "CREATE"
                            handleClose = {this.hideCreateFormModal}
                        />
                    </FormModal> 

                    <div id="allTask">
                        <div id="btn-plus">    
                        <input type="checkbox" id="cb" disabled></input>
                        <a onClick = {this.showCreateFormModal} id="tambah-task">Tambah Task <a id="plus">+</a></a>
                        </div>
                        
                        {this.state.tasks.map(
                            task =>
                                <Task  
                                    isUpdate = {this.state.isUpdate}
                                    showUpdateFormModal = {this.showUpdateFormModal}
                                    hideUpdateFormModal = {this.hideUpdateFormModal}
                                    removeTaskHandler = {this.removeTaskHandler}
                                    taskId = {task.id}
                                    description = {task.description}
                                />
                        )}
                    </div>
                </div>
            </div>
        )
    }
}

export default TaskContainer;