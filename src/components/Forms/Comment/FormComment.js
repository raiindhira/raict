import React, { Component } from 'react';
import Axios from 'axios';
import './FormComment.css';

class FormComment extends Component {
    constructor(props){
        super(props);
        this.state = {
            content: ""
        };
        this.handleContentChange = this.handleContentChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        if (this.props.action === "UPDATE") {
            await Axios.get(`http://localhost:8080/api/comments/${this.props.commentId}`)
                .then(response => {
                    const comment = response.data;
                    this.setState({
                        content: comment.content
                    });
                })
        }
    }

    handleContentChange(event) {
        this.setState({
            content: event.target.value
        });
    }

    async handleSubmit(event) {
        if (this.props.action === "CREATE") {
            const response = await fetch(`http://localhost:8080/api/taskCategories/${this.props.taskCategoryId}/comments/`, {
            method: 'POST',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin' : '*'
            },
            body: JSON.stringify(this.state)
        })
            .then (response => response.json())
    
            console.log(response);
        }

        if (this.props.action === "UPDATE") {
            const response = await fetch(`http://localhost:8080/api/taskCategories/comments/${this.props.commentId}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify(this.state)
            })
                .then(response => response.json())

        }

        this.setState({
            content: ''
        })

//        this.props.handleClose();
    }

    render() {
        return(
            <div className="Review">
                <h3 id="mnj">Dari Manajer</h3>
                <hr id="linemnj"/>
                <div className="my-card">
                    <form>
                        <div id="comment-box">
                            <textarea 
                                name = "content" 
                                id = "comments" 
                                placeholder = "Tulis komentar Anda disini..." 
                                onChange = {this.handleContentChange}
                                value = {this.state.content}    
                            />
                            <button type = "button" onClick = {this.handleSubmit}>Simpan</button>
                            {/* <div data-toggle="modal" data-target="#sukses">
                                <button id="simpan" type="button">Simpan</button>
                            </div> */}
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default FormComment;