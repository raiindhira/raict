import React, { Component } from 'react';
import Axios from 'axios';
import './FormMilestone.css';

class FormMilestone extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            startDate: '',
            dueDate: ''    
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartDateChange = this.handleStartDateChange.bind(this);
        this.handleDueDateChange = this.handleDueDateChange.bind(this);
    }

    async componentDidMount(){
        if(this.props.action === "UPDATE") {
            await Axios.get(`http://localhost:8080/api/milestones/${this.props.milestoneId}`)
                .then(response => {
                    const milestone = response.data;
                    this.setState ({
                        name: milestone.name,
                        startDate: milestone.startDate,
                        dueDate: milestone.endDate
                    })
                })
            // this.setState({
            //     // name: this.props.milestone.name,
            //     // startDate: this.props.milestone.startDate,
            //     // dueDate: this.props.milestone.dueDate
            // })
        } 
    }

    handleNameChange(event) {
        this.setState({
            name: event.target.value
        });
    }

    handleStartDateChange(event) {
        this.setState({
            startDate: event.target.value
        });
    }
    
    handleDueDateChange(event) {
        this.setState({
            dueDate: event.target.value
        });
    }

    async handleSubmit(event){

        if (this.props.action === "CREATE"){
            
            const response = await fetch('http://localhost:8080/api/projects/1/milestones/', {
            method: 'POST',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin' : '*'
            },
            body: JSON.stringify(this.state)
        })
            .then (response => response.json())
    
            console.log(response);
            await this.setState({
                name: '',
                startDate: '',
                dueDate: ''
            })
        }

        if (this.props.action === "UPDATE"){

            const response = await fetch(`http://localhost:8080/api/projects/milestones/${this.props.milestoneId}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify(this.state)
            })
                .then(response => response.json())

            await this.setState({
                name: '',
                startDate: '',
                dueDate: ''
            })
        }

        this.props.handleClose();

    }

    render() {
        return (
            <form>
            
                <label>
                    Name :
                    </label>
                <input type = "text"
                    name = "name"
                    value = {this.state.name}
                    onChange = {this.handleNameChange}
                    
                />
                
                
                <label>
                    Started Date : 
                    </label>
                <input type = "Date"
                    name = "startDate"
                    value = {this.state.startDate}
                    onChange = {this.handleStartDateChange}
                />
                

                <label>
                    End Date :
                    </label>
                <input type = "Date"
                    name = "dueDate"
                    value = {this.state.dueDate}
                    onChange = {this.handleDueDateChange}
                />
                
                
                <button type="button" id="simpanButton" onClick={this.handleSubmit}>Simpan</button>
            </form>
        )
    }

}

export default FormMilestone;