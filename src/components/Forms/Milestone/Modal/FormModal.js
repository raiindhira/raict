import React, { Component } from "react";
import "./Modal.css";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

const FormModal = ({ handleClose, flag, children, judul }) => {
  return (
    <Modal show={flag} onHide={handleClose}>
      <Modal.Header closeButton>
        <div className="modal-title">{judul}</div>
      </Modal.Header>
      <Modal.Body>{children}</Modal.Body>
    </Modal>
  );
};

export default FormModal;
