import React, { Component } from 'react';
import Axios from 'axios';
import './FormTask.css'

class FormTask extends Component {
    constructor(props){
        super(props);
        this.state = {
            description: ""
        }
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        if (this.props.action === "UPDATE") {
            await Axios.get(`http://localhost:8080/api/tasks/${this.props.taskId}`)
                .then(response => {
                    const task = response.data;
                    this.setState({
                        description: task.description
                    });
                })
        }
    }

    handleDescriptionChange(event) {
        this.setState({
            description: event.target.value
        });
    }

    async handleSubmit(event) {
        if (this.props.action === "CREATE") {
            const response = await fetch (`http://localhost:8080/api/taskCategories/${this.props.taskCategoryId}/tasks`, {
                method: 'POST',
                headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin' : '*'
            },
            body: JSON.stringify(this.state)
        })
            .then (response => response.json())

            await this.setState({
                description: ''
            })
        }

        if (this.props.action === "UPDATE") {
            const response = await fetch(`http://localhost:8080/api/taskCategories/tasks/${this.props.taskId}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify(this.state)
            })
                .then(response => response.json())

                await this.setState({
                    description: ''
                })
        }

        this.props.handleClose();
    }

    render() {
        return (
            <div className="FormTask">  
            <form>
                <h4 id="tbh-t">Task Detail</h4>
                <hr id="line-ft"></hr>
                <div id="ft">
                <label id="dsc-task">
                    Deskripsi Task : <br></br>
                    <input type = "text"
                        name = "description"
                        value = {this.state.description}
                        onChange = {this.handleDescriptionChange}
                    id="inp-task" required/>
                </label>
                </div>

                <button id="simpan-task" onClick = {this.handleSubmit}>Submit</button>
            </form>
            </div>
        )
    }
}

export default FormTask;