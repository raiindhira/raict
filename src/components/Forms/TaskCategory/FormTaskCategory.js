import React, { Component } from 'react';
import Axios from 'axios';

class FormTaskCategory extends Component {

    constructor(props){
        super(props);
        this.state = {
            name: '',
            dueDate: '',
            users: [{ }],
            allUsers: [{id:'1',
                        name:'rai'},
                        {id: '2',
                        name:'ilham'},
                        {id:'3',
                        name:'ricky'}]
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleDueDateChange = this.handleDueDateChange.bind(this);
    }

    async componentDidMount() {
        if (this.props.action === "UPDATE") {
            console.log("id yang mau di update" + this.props.id)
            await Axios.get(`http://localhost:8080/api/taskCategories/${this.props.id}`)
                .then(response => {
                    const taskCategory = response.data;
                    this.setState({
                        name: taskCategory.name,
                        dueDate: taskCategory.dueDate
                    })
                })
        }
    }

    handleNameChange(event) {
        this.setState({
            name: event.target.value
        });
    }

    handleDueDateChange(event) {
        this.setState({
            dueDate: event.target.value
        })
    }

    getId(item, index){
        var idUser = [item.id];
        return idUser;
    }

    

    async handleSubmit(event) {
        this.setState({
            users: this.state.users.map(this.getId)
        })
        console.log ("halo state telah diubah");
        console.log(this.state.users);
        if (this.props.action === "CREATE") {
            console.log('cihuy');
            console.log(this.state.users);
            const response = await fetch (`http://localhost:8080/api/milestones/${this.props.milestoneId}/taskCategories/`, {
                method: 'POST',
                headers: {
                    'Accept' : 'application/json',
                    'Content-Type' : 'application/json',
                    'Access-Control-Allow-Origin' : '*'
                },
                body: JSON.stringify(this.state)
            })
                .then(response => response.json())

                await this.setState({
                    name: '',
                    dueDate: '',
                    users: [{  }],
                })
        }

        if (this.props.action === "UPDATE"){
            const response = await fetch(`http://localhost:8080/api/milestones/taskCategories/${this.props.id}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify(this.state)
            })
                .then(response => response.json())

            await this.setState({
                name: '',
                dueDate: ''
            })
        }

        this.props.handleClose()
    }
    handleMemberNameChange = idx => evt => {
        const newMember = this.state.users.map((member, sidx) => {
          if (idx !== sidx) return member;
          return { ...member, id: evt.target.value };
        });
    
        this.setState({ users: newMember });
      };
    
     
    
      handleAddMember = () => {
        this.setState({
            users: this.state.users.concat([{ }])
        });
        console.log(this.state.users);
      };
    
      handleRemoveMember = idx => () => {
        console.log("AAAAAA");
        console.log(this.allUsers);
        this.setState({
            users: this.state.users.filter((s, sidx) => idx !== sidx)
        });
      };





    render() {
        return (
            <form>
                <label>
                    Nama Task Category :
                    <input type = "text"
                        name = "name"
                        value = {this.state.name}
                        onChange = {this.handleNameChange}
                    />
                </label>
                <label>
                    Due Date :
                    <input type = "Date"
                        name = "dueDate"
                        value = {this.state.dueDate}
                        onChange = {this.handleDueDateChange}
                    />
                </label>
                <label>
                    Anggota :
                    <h5>*</h5>
                    {this.state.users.map((member, idx) => (
                    <div className="member">
                        

                        <select onChange={this.handleMemberNameChange(idx)}>
                            <option selected = 'selected'>default </option>
                            { this.state.allUsers.map(
                            user => 
                            <option value={user.id}> {user.name}</option>
                            )}
                            
                            
                            
                        </select>
                        <button
                        type="button"
                        onClick={this.handleRemoveMember(idx)}
                        className="small"
                        >
                        -
                        </button>

                        
                    </div>
                ))}

                <button
                    type="button"
                    onClick={this.handleAddMember}
                    className="small"
                    >
                    Add Member
                </button>

                </label>



                <button type="button" onClick={this.handleSubmit}>SUBMIT</button>
            </form>
        )
    }
}

export default FormTaskCategory;