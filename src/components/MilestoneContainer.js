import React, { Component, Fragment } from 'react';

import axios from 'axios';
import ModalIlham from './Modal';
import UserForm from '../components/UserForm';
import TaskCategoryForm from '../components/TaskCategoryForm'
import Modal from 'react-modal';
import { ModalProvider, ModalConsumer } from './ModalContext';
import ModalRoot from './ModalRoot';

class MilestoneContainer extends Component{
    constructor(props){
        super(props);
        this.state = {
            action: "CREATE",
            user: {
                id: '',
                email : '',
                fullName : '',
                nip : '',
                password : '',
                roleId : ''
            },
            projectId: 1,
            milestones: [],
            show: false
            
        };
        this.updateHandler = this.updateHandler.bind(this);
        this.removeHandler = this.removeHandler.bind(this);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.updateMilestone = this.updateMilestone.bind(this);
    }

    showModal = () => {
        this.setState({ show: true });
    };
    
    hideModal = () => {
     this.setState({ show: false });
    };

    userFormModal = () =>{

    };

    async componentDidMount() {
        await axios.get(`http://localhost:8080/api/users/`)
            .then(response => {
                const milestones = response.data;
                this.setState ({ milestones });
                console.log(response);
            })
            console.log("PROPENSI <3");
        console.log(this.state.milestones);
    }

    updateMilestone(milestones){
        this.setState ({ milestones });
    }

    async removeHandler(event, milestone) {
        await fetch(`http://localhost:8080/api/users/${milestone.id}`, {
            method: 'DELETE',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            }
        }).then(() => {
            let updatedMilestones = [...this.state.milestones].filter(i => i.id !== milestone.id);
            this.setState({milestones: updatedMilestones});
        });
    };

    async updateHandler(event, milestone) {
        //Reference to the same object, so when the updatedMilestone changed, this.state.user was changed
        let updatedMilestone = this.state.user;
        console.log("SET STATE ACTION");

        this.setState({
            action: "UPDATE",
        })

        updatedMilestone.id = milestone.id
        updatedMilestone.email = milestone.email;
        updatedMilestone.fullName = milestone.fullName;
        updatedMilestone.nip = milestone.nip;
        updatedMilestone.password = milestone.password;
        updatedMilestone.roleId = milestone.role.id;
    }


    render(){
        return(
            <div> 
                <ul>
                    { this.state.milestones.map(
                        milestone => 
                            <li>
                                { milestone.fullName }
                                <button type = "submit" onClick = {event => this.updateHandler(event, milestone)}>EDIT</button>
                                <button type = "submit" onClick = {event => this.removeHandler(event, milestone)}>DELETE</button>
                            </li>
                            
                    )}
                </ul>

               
                
                <ModalIlham show={this.state.show} handleClose={this.hideModal}>
                    
                    <UserForm 
                    action = {this.state.action}
                    user = {this.state.user}
                    handleClose={this.hideModal}
                    renderUpdate={this.updateMilestone}
                    />
                    
                </ModalIlham>
                
                <button type="button" onClick={this.showModal}>
                    open
                </button>
                <button type="button" onClick={this.showModal}>
                    open 2
                </button>
                <TaskCategoryForm />
                
                
            </div>
        );
    }
}


export default MilestoneContainer;