import React from 'react';

const Modal = ({ handleClose, show, children }) => {
    const showHideClassName =  show ? "modal display-block" : "modal display-none";

    return (
        <main>
            <div className={showHideClassName}>
                <section className="modal-main">
                    {children}
                    <button onClick={handleClose}>Close</button>
                </section>
            </div>
        </main>
    )
}

export default Modal;