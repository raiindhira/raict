import React, { Component } from 'react';
import "./ProjectAttributes.css";
import Axios from 'axios';

class ProjectAttributes extends Component {
    constructor(props){
        super(props);
        this.state = {
            picUser: "",
            startDate: "",
            endDate: "",
            projectValue: "Rp90.000.000",
            users: [
                {
                    fullName: "Ricky Chandra"
                },
                {
                    fullName: "Ilham Pamungkas"
                }
            ]            
        }
        this.fetchAll = this.fetchAll.bind(this);
        this.fetchProjectAttributes = this.fetchProjectAttributes.bind(this);
        // this.fetchProjectUsers = this.fetchProjectUsers.bind(this);
    }

    componentDidMount() {
        this.fetchAll();
    }

    async fetchAll () {
        await this.fetchProjectAttributes(this.props.projectId);
        // await this.fetchProjectUsers(this.props.projectId);
    }

    async fetchProjectAttributes(projectId) {
        await Axios.get(`http://localhost:8080/api/projects/${projectId}`)
            .then(response => {
                const project = response.data;
                this.setState ({
                    picUser: project.picuser.fullName,
                    startDate: project.startDate,
                    endDate: project.endDate
                })
                console.log(this.state)
            })
    }

    render() {
        return (
            <div id="team-member">
                <div className="row">
                    <div className="col md-3">
                        <b id="pm">PROJECT MANAGER</b>
                        <p id="pm-name">{this.state.picUser}</p>
                    </div>
                    <div className="col md-3">
                        <b id="pv">NILAI PROYEK</b>
                        <p id="value">{this.state.projectValue}</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col md-3">
                        <b id="wm">WAKTU MULAI</b>
                        <p id="start">{this.state.startDate}</p>
                        <b id="wB">WAKTU BERAKHIR</b>
                        <p id="end">{this.state.endDate}</p>
                    </div>
                    <div className="col md-3">
                        <b id="team">ANGGOTA PROYEK</b>
                        <ul>
                            { this.state.users.map(
                                users => 
                                <li id="t-name">
                                    {users.fullName}
                                </li>
                            
                            )}
                        </ul>
                    </div>
                </div>
                <div className="row">
                    <div className="col md-3">
                    <b id="total">NILAI AKHIR</b>
                    <p id="score">89.20</p>
                    </div>
                    <div className="col md-3"></div>
                </div>
            </div>
        )
    }
}

export default ProjectAttributes;