import React, { Component } from 'react';
import "./ProjectDescription.css";
import Axios from 'axios';

class ProjectDescription extends Component {
    constructor(props){
        super(props);
        this.state = {
            description: ""
        }
    }

    componentDidMount(){
        Axios.get(`http://localhost:8080/api/projects/${this.props.projectId}`)
            .then(response => {
                const project = response.data;
                const projectDescription = project.description;
                this.setState ({description: projectDescription});
                // console.log(this.state.description);
            })
    }

    render() {
        return (
            <div className="ProjectDescription">
                <div className="grid">
                <div id="prj-desc">
                    <h6 id="dp">Deskripsi Proyek</h6>
                    <p id="desc">
                        {this.state.description}
                    </p>
                </div>
            {/* <div id="team-member">
                <div className="row">
                    <div className="col md-3">
                        <b id="pm">PROJECT MANAGER</b>
                        <p id="pm-name">Ilham Pamungkas</p>
                    </div>
                    <div className="col md-3">
                        <b id="pv">NILAI PROYEK</b>
                        <p id="value">Rp90.000.000</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col md-3">
                        <b id="wm">WAKTU MULAI</b>
                        <p id="start">12 Januari 2018</p>
                        <b id="wB">WAKTU BERAKHIR</b>
                        <p id="end">12 Januari 2019</p>
                    </div>
                    <div className="col md-3">
                        <b id="team">ANGGOTA PROYEK</b>
                        <li id="t-name">Ilham Pamungkas</li>
                        <li id="t-name">Ilham Pamungkas</li>
                    </div>
                </div>
                <div className="row">
                    <div className="col md-3">
                    <b id="total">NILAI AKHIR</b>
                    <p id="score">89.20</p>
                    </div>
                    <div className="col md-3"></div>
                </div>
            </div> */}
            </div>
        </div>
    );
  }
}

export default ProjectDescription;