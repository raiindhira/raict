import React, { Component } from 'react';

export default class TaskCategoryForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            nama: '',
            dueDate: '',
            members: [{ name: "" }],
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleMemberNameChange = this.handleMemberNameChange.bind(this);
        this.handleAddMember = this.handleAddMember.bind(this);
        this.handleRemoveMember = this.handleRemoveMember.bind(this);
    }

    handleNameChange(event) {
        this.setState({
            nama: event.target.value
        });
    }

    handleMemberNameChange = idx => evt => {
        const newMember = this.state.members.map((member, sidx) => {
          if (idx !== sidx) return member;
          return { ...member, name: evt.target.value };
        });
    
        this.setState({ members: newMember });
      };
    
     
    
      handleAddMember = () => {
        this.setState({
          members: this.state.members.concat([{ name: "" }])
        });
        console.log(this.state.members);
      };
    
      handleRemoveMember = idx => () => {
        this.setState({
          members: this.state.members.filter((s, sidx) => idx !== sidx)
        });
      };


    render(){
        return(
            <form>
                <label>
                    Nama Task Category :
                    <input type= "text"
                        name = "name"
                        value = {this.state.nama}
                        onChange = {this.handleNameChange}
                    />
                </label>
                <label>
                    Due Date:
                    <input type= "date"
                        name = "due date"
                        value = {this.state.dueDate}
                        onChange = {this.handleName}
                    />
                </label>
               

                {this.state.members.map((member, idx) => (
                    <div className="member">
                        

                        <select onChange={this.handleMemberNameChange(idx)}>
                            <option selected = 'selected'>default </option>
                            <option value='1'> ilham </option>
                            <option value='2'> ricky </option>
                        </select>
                        <button
                        type="button"
                        onClick={this.handleRemoveMember(idx)}
                        className="small"
                        >
                        -
                        </button>

                        
                    </div>
                ))}

                <button
                    type="button"
                    onClick={this.handleAddMember}
                    className="small"
                    >
                    Add Shareholder
                </button>




                
                    
            </form>
        )
            
        
    }

}