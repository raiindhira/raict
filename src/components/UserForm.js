import React, { Component } from 'react';
import axios from 'axios';

class UserForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email : '',
            fullName : '',
            nip : '',
            password : '',
            roleId : ''    
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleFullNameChange = this.handleFullNameChange.bind(this);
        this.handleNIPChange = this.handleNIPChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleRoleIdChange = this.handleRoleIdChange.bind(this);
    }

    componentDidMount() {
        axios.get(`http://localhost:8080/api/users/`)
            .then(response => {
                const milestones = response.data;
                this.props.renderUpdate(milestones);
                console.log(response);
            })
    }



    handleEmailChange(event) {
        this.setState({
            email: event.target.value
        });
    }

    handleFullNameChange(event) {
        this.setState({
            fullName: event.target.value
        });
    }
    
    handleNIPChange(event) {
        this.setState({
            nip: event.target.value
        });
    }

    handlePasswordChange(event) {
        this.setState({
            password: event.target.value
        });
    }

    handleRoleIdChange(event) {
        this.setState({
            roleId: event.target.value
        });
    }

    async handleSubmit(event){
        console.log(this.state.fullName);
        if (this.props.action === "CREATE"){
            
            const response = await fetch('http://localhost:8080/api/users/', {
            method: 'POST',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin' : '*'
            },
            body: JSON.stringify(this.state)
        })
            .then (response => response.json())
    
            await this.setState({
                email : '',
                fullName : '',
                nip : '',
                password : '',
                roleId : '' 
            })
        }

        if (this.props.action === "UPDATE"){
            this.setState({
                email: this.props.user.email,
                fullName: this.props.user.fullName,
                nip: this.props.user.nip,
                password: this.props.user.password,
                roleId: this.props.user.roleId
            })
            const response = await fetch(`http://localhost:8080/api/users/${this.props.user.id}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify(this.state)
            })
                .then(response => response.json())

            await this.setState({
                email : '',
                fullName : '',
                nip : '',
                password : '',
                roleId : '' 
            })
        }

        this.props.handleClose();
        
        alert('sucess');
        window.location.reload();

        
    }

    render() {
        return (
            <form>
                <label>
                    Email :
                <input type = "email"
                    name = "email"
                    value = {this.state.email}
                    onChange = {this.handleEmailChange}
<<<<<<< HEAD
                    
=======
                    placeholder = {this.props.user.email}
>>>>>>> f5448e1c32433d8d1ea99d80b6ebb03a7506c76b
                />
                </label>
                
                <label>
                    Full Name :
                <input type = "timestamp"
                    name = "fullName"
                    value = {this.state.fullName}
                    onChange = {this.handleFullNameChange}
                    placeholder = {this.props.user.fullName}
                />
                </label>

                <label>
                    NIP : 
                <input type = "nip"
                    name = "nip"
                    value = {this.state.nip}
                    onChange = {this.handleNIPChange}
                    placeholder = {this.props.user.nip}
                />
                </label>
                
                <label>
                    Password :
                <input type = "password"
                    name = "password"
                    value = {this.state.password}
                    onChange = {this.handlePasswordChange}
                    placeholder = {this.props.user.password}
                />
                </label>

                <label>
                    Role :
                <input type = "text"
                    name = "roleId"
                    value = {this.state.roleId}
                    onChange = {this.handleRoleIdChange}
                    placeholder = {this.props.user.roleId}
                />
                </label>

                <button type="button" onClick={this.handleSubmit}>SUBMIT</button>
            </form>
        )
    }
}

export default UserForm;