import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import * as serviceWorker from './serviceWorker';
import { Router } from '@reach/router';

import ProjectDetail from './pages/ProjectDetail/ProjectDetail.js';
import TaskCategoryDetail from './pages/TaskCategoryDetail/TaskCategoryDetail.js';

ReactDOM.render(
    (
        <Router>
            <ProjectDetail path="/project"/>
            <TaskCategoryDetail path="/task-category"/>
        </Router>
        
    )
    
    , document.getElementById('root'));

export default
serviceWorker.unregister();
