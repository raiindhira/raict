import React, { Component } from "react";
// import SideBar from '../../components/Sidebar/SideBar.js';
// import HeaderProjectDetail from '../../components/Headers/ProjectDetail/HeaderProjectDetail';
// import Role from '../../components/Headers/ProjectDetail/Role/Role.js';
import SideBar from "../../components/Base/SideBar/SideBar.js";
import ProjectDescription from "../../components/ProjectDescription/ProjectDescription.js";
import ProjectAttributes from "../../components/ProjectAttributes/ProjectAttributes.js";
import RealMilestoneContainer from "../../components/Containers/Milestones/RealMilestoneContainer.js";
import "./ProjectDetail.css";

class ProjectDetail extends Component {
  render() {
    return (
      <div className="container row">
        <SideBar />
        <div className="col-md-8">
          <ProjectDescription projectId={1} />
          <ProjectAttributes projectId={1} />
          <RealMilestoneContainer />
        </div>
      </div>
    );
  }
}

export default ProjectDetail;
