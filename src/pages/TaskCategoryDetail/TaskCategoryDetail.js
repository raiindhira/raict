import React, { Component } from 'react';
import SideBar from '../../components/Base/SideBar/SideBar.js'
import TaskContainer from '../../components/Containers/Tasks/TaskContainer.js';
import CommentContainer from '../../components/Containers/Comments/CommentContainer.js';

class TaskCategoryDetail extends Component {
    render(){
        return (
            <div>
                <SideBar/>
                {/* <TaskCategoryAttributes/> */}
                {/* <TaskCategoryMembers/> */}
                <TaskContainer taskCategoryId = {2}/>
                {/* <SubmissionContainer/> */}
                <CommentContainer taskCategoryId = {2}/>
                {/* <Score/> */}
            </div>
        )
    }
}

export default TaskCategoryDetail;