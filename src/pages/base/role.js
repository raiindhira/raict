import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './role.css';

export default class Role extends Component {
  render() {
     return (
         <div className="Role">
            <h6 id="name">Ilham Pamungkas
            <img src="https://image.flaticon.com/icons/svg/876/876779.svg" id="out" alt="signout" title="Log Out"></img>
            </h6>
            <p id="level">CEO</p>
         </div>
        );
    }
  }