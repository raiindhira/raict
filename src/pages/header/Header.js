import React, { Component } from 'react';
import "./Header.css"

export default class Header extends Component {
  render() {
    return (
        <div className="Header">
            <nav className="crumbs">
                <li className="crumb"><a href="#">Home </a></li>
                <li className="crumb"><a href="#">Daftar Proyek </a></li>
                <li className="crumb"><a href="#">Proyek PPL </a></li>
                <li className="crumb"><a href="#">Analisis </a></li>
                <li className="crumb">Laporan 1</li>
            </nav>
            <h5 id="page-title">Proyek PPL</h5>
        </div>
    );
  }
}